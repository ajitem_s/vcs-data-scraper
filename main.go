package main

import (
	"bytes"
	"context"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/go-cmd/cmd"
	"github.com/gocarina/gocsv"
	"github.com/mafredri/cdp"
	"github.com/mafredri/cdp/devtool"
	"github.com/mafredri/cdp/protocol/dom"
	"github.com/mafredri/cdp/protocol/page"
	"github.com/mafredri/cdp/protocol/target"
	"github.com/mafredri/cdp/rpcc"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

type Company struct {
	Name          string `csv:"company_name"`
	Link          string `csv:"-"`
	Industry      string `csv:"company_industry"`
	CompanyType   string `csv:"company_type"`
	Address       string `csv:"address"`
	City          string `csv:"company_city"`
	State         string `csv:"company_state"`
	PinCode       int64  `csv:"company_pin_code"`
	ContactPerson string `csv:"company_contact_person"`
	MobileNumber  string `csv:"company_mobile_number"`
	PhoneNumber   string `csv:"company_phone_number"`
	LevelOfOffice string `csv:"company_level_of_office"`
	Website       string `csv:"website"`
}

type Companies []Company

func main() {
	chromeCmd := cmd.NewCmd(
		"/home/ajitem/Downloads/stable-headless-chromium-amazonlinux-2017-03/headless-chromium",
		"--remote-debugging-port=9222", "--disable-gpu", "--no-sandbox",
	)
	statusChan := chromeCmd.Start()
	// wait for chrome to launch
	time.Sleep(5 * time.Second)

	var categories Categories
	var baseURL = "https://vcsdata.com"

	go func() {
		// connect to chrome
		chromeTarget, err := connect(120 * time.Second)
		if err != nil {
			panic(err)
		}

		// open a new tab
		id, err := openTab(chromeTarget, 120*time.Second)
		if err != nil {
			panic(err)
		}

		// load website
		html, err := run("ws://127.0.0.1:9222/devtools/page/"+id, baseURL, 120*time.Second)
		if err != nil {
			panic(err)
		}
		// close tab
		_ = closeTab(chromeTarget, id, 120*time.Second)

		// prepare to parse HTML
		doc, err := goquery.NewDocumentFromReader(bytes.NewReader([]byte(html)))
		if err != nil {
			panic(err)
		}

		// get categories
		doc.Find("#tab-1 .listing a").Each(func(i int, s *goquery.Selection) {
			var category Category

			category.Name = strings.Trim(s.Text(), " ")

			link, _ := s.Attr("href")
			link = strings.Replace(link, " ", "%20", -1)

			category.Link = fmt.Sprintf("%s/%s", baseURL, link)

			// add category to list of categories
			categories = append(categories, category)
		})

		var pages = make(chan map[int64]int64)

		for index, category := range categories {
			go func(index int, category Category) {
				// open a new tab
				catTabId, err := openTab(chromeTarget, 120*time.Second)
				if err != nil {
					panic(err)
				}

				// open category page
				innerHtml, err := run(
					"ws://127.0.0.1:9222/devtools/page/"+catTabId,
					category.Link,
					120*time.Second,
				)
				if err != nil {
					panic(err)
				}
				// close current category tab
				_ = closeTab(chromeTarget, catTabId, 120*time.Second)

				// find out total number of pages
				innerDoc, err := goquery.NewDocumentFromReader(bytes.NewReader([]byte(innerHtml)))
				if err != nil {
					panic(err)
				}

				// find second last element of pagination
				var pageInfo = make(map[int64]int64)

				pageHrefEle := innerDoc.Find(".pagination a.next").Prev()
				if pageHrefEle != nil {
					// extract page URL
					pageHref, _ := pageHrefEle.Attr("href")
					// split query parameters by '&'
					pageHrefArr := strings.Split(pageHref, "&")
					// last parameter has page number value
					pageNoArr := strings.Split(pageHrefArr[len(pageHrefArr)-1], "=")
					log.Println("Category:", category.Name, "Pages:", pageNoArr)
					// if pages are available
					if len(pageNoArr) == 2 {
						// parse total number of pages
						pageInfo[int64(index)], _ = strconv.ParseInt(pageNoArr[1], 10, 64)
					}
				}
				pages <- pageInfo
			}(index, category)

		}

		var wait = make(chan bool)
		go func() {
			for i := 0; i < len(categories); i++ {
				pageInfo := <-pages

				log.Println("received page info:", pageInfo)

				for index, pages := range pageInfo {
					categories[index].Pages = pages
				}

				wait <- true
			}
		}()

		var scraping = make(chan int)
		go func() {
			for i := 0; i < len(categories); i++ {
				// block until one category is finished
				<- wait
			}

			// iterate over each category and scrape company information for each
			for index, category := range categories {
				go func(index int, category Category) {
					// open a new tab
					catTabId, err := openTab(chromeTarget, 120*time.Second)
					if err != nil {
						panic(err)
					}

					// start with page number 1 and continue till last page of current category
					var pageNumber int64
					for pageNumber = 1; pageNumber < category.Pages; pageNumber++ {
						// load first page of the category
						innerHtml, err := run(
							"ws://127.0.0.1:9222/devtools/page/"+catTabId,
							category.Link+"&page="+strconv.FormatInt(pageNumber, 10),
							150*time.Second,
						)
						if err != nil {
							panic(err)
						}

						// start parsing
						log.Printf("PAGE %d of %d", pageNumber, category.Pages)
						log.Println("URL ", category.Link+"&page="+strconv.FormatInt(pageNumber, 10), )

						// prepare to parse
						innerDoc, err := goquery.NewDocumentFromReader(bytes.NewReader([]byte(innerHtml)))
						if err != nil {
							panic(err)
						}

						// find content listing block from the page to get company name and link
						innerDoc.Find("#company_list .contentlisting").Each(func(i int, s *goquery.Selection) {
							var company Company
							if s.Find(".innertitle a").Text() != "" {
								c := s.Find(".innertitle a")
								if c.Text() != "" {
									// set company name
									company.Name = c.Text()

									// set company link
									link, _ := c.Attr("href")
									link = strings.Replace(link, " ", "%20", -1)

									company.Link = fmt.Sprintf("%s/%s", baseURL, link)

									// set company city / zipcode
									s.Find(".content .list").Each(func(i int, s *goquery.Selection) {
										if i == 0 {
											company.Industry = s.Find(".span75").Text()
										} else {
											location := strings.Split(s.Find(".span75").Text(), "-")
											if len(location) == 2 {
												company.City = strings.Trim(location[0], " ")
												company.PinCode, _ = strconv.ParseInt(
													strings.Trim(location[1], " "),
													10,
													64,
												)
											}
										}
									})

									// set company type and level of office
									s.Find(".content .list1").Each(func(i int, s *goquery.Selection) {
										if i == 0 {
											company.CompanyType = s.Find(".span5").Text()
										} else {
											company.LevelOfOffice = s.Find(".span5").Text()
										}
									})

									// visit company details page, scrape info and populate company object
									go func(index int, company Company) {
										// open a new tab
										companyTabId, err := openTab(chromeTarget, 300*time.Second)
										if err != nil {
											panic(err)
										}
										// load company details page
										companyHtml, err := run(
											"ws://127.0.0.1:9222/devtools/page/"+companyTabId,
											company.Link,
											240*time.Second,
										)
										if err != nil {
											panic(err)
										}
										// close tab
										_ = closeTab(chromeTarget, companyTabId, 120*time.Second)

										// prepare to parse
										companyDoc, err := goquery.NewDocumentFromReader(bytes.NewReader([]byte(companyHtml)))
										if err != nil {
											panic(err)
										}

										// parse details about company
										companyDoc.Find(".rightsection .content .detailbox").
											Each(func(i int, s *goquery.Selection) {
												if i == 0 {
													// extract address
													company.Address = s.Find(".content .listRow .span9 span").Text()

													// extract website
													s.Find(".content .list").Each(func(i int, s *goquery.Selection) {
														if i == 1 {
															company.Website = s.Find(".span7").Text()
														}
													})

													// extract state
													s.Find(".content .list1").Each(func(i int, s *goquery.Selection) {
														if i == 1 {
															company.State = s.Find(".span7").Text()
														}
													})
												}

												if i == 2 {
													s.Find(".content .listRow .span8").Each(func(i int, s *goquery.Selection) {
														if i == 0 {
															company.ContactPerson = s.Text()
														} else if i == 1 {
															company.MobileNumber = s.Text()
														} else {
															company.PhoneNumber = s.Text()
														}
													})
												}
											})

										// push current company to list of companies
										categories[index].Companies = append(categories[index].Companies, company)
									}(index, company)
								}
							}
						})
					}

					// close current category tab
					_ = closeTab(chromeTarget, catTabId, 120*time.Second)

					// push to channel to indicate successful parsing of one category
					scraping <- index
				}(index, category)
			}
		}()

		go func() {
			for i := 0; i < len(categories); i++ {
				// block until one category is finished
				index := <-scraping

				// prepare filename for csv
				filename := strings.Trim(
					strings.Replace(
						strings.Replace(strings.ToLower(categories[index].Name), "/", "", -1),
						" ",
						"_",
						-1,
					),
					" ",
				)

				// open file
				companiesFile, err := os.OpenFile(filename+".csv", os.O_RDWR|os.O_CREATE, os.ModePerm)
				if err != nil {
					panic(err)
				}

				// write finished category to file
				err = gocsv.MarshalFile(&categories[index].Companies, companiesFile) // Use this to save the CSV back to the file
				if err != nil {
					panic(err)
				}

				// close file
				_ = companiesFile.Close()
			}

			// terminate chrome
			log.Println(chromeCmd.Stop())
		}()
	}()

	// block until scraping is over and chrome is terminated
	_ = <-statusChan

}

func connect(timeout time.Duration) (*devtool.Target, error) {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	// Use the DevTools HTTP/JSON API to manage targets (e.g. pages, webworkers).
	dt := devtool.New("http://127.0.0.1:9222")
	pt, err := dt.Get(ctx, devtool.Page)
	if err != nil {
		pt, err = dt.Create(ctx)
		if err != nil {
			return nil, err
		}
	}
	return pt, nil
}

func openTab(chromeTarget *devtool.Target, timeout time.Duration) (target.ID, error) {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	// Initiate a new RPC connection to the Chrome DevTools Protocol target.
	conn, err := rpcc.DialContext(ctx, chromeTarget.WebSocketDebuggerURL)
	if err != nil {
		return "", err
	}
	defer conn.Close() // Leaving connections open will leak memory.

	c := cdp.NewClient(conn)

	createCtx, err := c.Target.CreateBrowserContext(ctx)
	if err != nil {
		return "", err
	}

	createTargetArgs := target.NewCreateTargetArgs("about:blank").
		SetBrowserContextID(createCtx.BrowserContextID)

	createTarget, err := c.Target.CreateTarget(ctx, createTargetArgs)
	if err != nil {
		return "", err
	}

	return createTarget.TargetID, nil
}

func closeTab(chromeTarget *devtool.Target, id target.ID, timeout time.Duration) error {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	// Initiate a new RPC connection to the Chrome DevTools Protocol target.
	conn, err := rpcc.DialContext(ctx, chromeTarget.WebSocketDebuggerURL)
	if err != nil {
		return err
	}
	defer conn.Close() // Leaving connections open will leak memory.

	c := cdp.NewClient(conn)

	_, err = c.Target.CloseTarget(ctx, target.NewCloseTargetArgs(id))
	return err
}

func run(id target.ID, url string, timeout time.Duration) (string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	conn, err := rpcc.DialContext(ctx, string(id))
	if err != nil {
		return "", err
	}
	defer conn.Close()

	// This cdp client controls the "incognito tab".
	c := cdp.NewClient(conn)

	// Open a DOMContentEventFired client to buffer this event.
	domContent, err := c.Page.DOMContentEventFired(ctx)
	if err != nil {
		return "", err
	}
	defer domContent.Close()

	// Enable events on the Page domain, it's often preferrable to create
	// event clients before enabling events so that we don't miss any.
	if err = c.Page.Enable(ctx); err != nil {
		return "", err
	}

	// Create the Navigate arguments with the optional Referrer field set.
	navArgs := page.NewNavigateArgs(url)
	nav, err := c.Page.Navigate(ctx, navArgs)
	if err != nil {
		return "", err
	}

	// Wait until we have a DOMContentEventFired event.
	if _, err = domContent.Recv(); err != nil {
		return "", err
	}

	// wait for ajax to render
	time.Sleep(5 * time.Second)

	fmt.Printf("Page loaded with frame ID: %s\n", nav.FrameID)

	// Fetch the document root node. We can pass nil here
	// since this method only takes optional arguments.
	doc, err := c.DOM.GetDocument(ctx, nil)
	if err != nil {
		return "", err
	}

	// Get the outer HTML for the page.
	result, err := c.DOM.GetOuterHTML(ctx, &dom.GetOuterHTMLArgs{
		NodeID: &doc.Root.NodeID,
	})
	if err != nil {
		return "", err
	}

	return result.OuterHTML, nil
}
