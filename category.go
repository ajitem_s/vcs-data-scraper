package main

import (
	"bytes"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/mafredri/cdp/devtool"
	"log"
	"strconv"
	"strings"
	"time"
)

type Category struct {
	Name      string
	Link      string
	Pages     int64
	Companies Companies
}

type Categories []Category

func GetCategories(chromeTarget *devtool.Target, baseURL string) (categories Categories) {
	// open a new tab
	id, err := openTab(chromeTarget, 120*time.Second)
	if err != nil {
		panic(err)
	}

	// load website
	html, err := run("ws://127.0.0.1:9222/devtools/page/"+id, baseURL, 120*time.Second)
	if err != nil {
		panic(err)
	}
	// close tab
	_ = closeTab(chromeTarget, id, 120*time.Second)

	// prepare to parse HTML
	doc, err := goquery.NewDocumentFromReader(bytes.NewReader([]byte(html)))
	if err != nil {
		panic(err)
	}

	// get categories
	doc.Find("#tab-1 .listing a").Each(func(i int, s *goquery.Selection) {
		var category Category

		category.Name = strings.Trim(s.Text(), " ")

		link, _ := s.Attr("href")
		link = strings.Replace(link, " ", "%20", -1)

		category.Link = fmt.Sprintf("%s/%s", baseURL, link)

		// add category to list of categories
		categories = append(categories, category)
	})

	var pages = make(chan map[int64]int64)

	for index, category := range categories {
		go getCategoryPages(chromeTarget, index, category, pages)
	}

	for i := 0; i < len(categories); i++ {
		pageInfo := <-pages

		for index, pages := range pageInfo {
			categories[index].Pages = pages
		}
	}

	return
}

func getCategoryPages(chromeTarget *devtool.Target, index int, category Category, pages chan map[int64]int64) {
	// open a new tab
	catTabId, err := openTab(chromeTarget, 120*time.Second)
	if err != nil {
		panic(err)
	}

	// open category page
	innerHtml, err := run(
		"ws://127.0.0.1:9222/devtools/page/"+catTabId,
		category.Link,
		120*time.Second,
	)
	if err != nil {
		panic(err)
	}
	// close current category tab
	_ = closeTab(chromeTarget, catTabId, 120*time.Second)

	// find out total number of pages
	innerDoc, err := goquery.NewDocumentFromReader(bytes.NewReader([]byte(innerHtml)))
	if err != nil {
		panic(err)
	}

	// find second last element of pagination
	var pageInfo = make(map[int64]int64)
	pageHrefEle := innerDoc.Find(".pagination a.next").Prev()
	if pageHrefEle != nil {
		// extract page URL
		pageHref, _ := pageHrefEle.Attr("href")
		// split query parameters by '&'
		pageHrefArr := strings.Split(pageHref, "&")
		// last parameter has page number value
		pageNoArr := strings.Split(pageHrefArr[len(pageHrefArr)-1], "=")
		log.Println("Category:", category.Name, "Pages:", pageNoArr)
		// if pages are available
		if len(pageNoArr) == 2 {
			// parse total number of pages
			pageInfo[int64(index)], _ = strconv.ParseInt(pageNoArr[1], 10, 64)
		}
	}
	pages <- pageInfo
}
