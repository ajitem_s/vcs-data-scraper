package main

import (
	"bitbucket.org/ajitem_s/vcs-data-scraper/chrome"
	"fmt"
	"log"
	"time"
)

func main() {
	var browser chrome.Chrome

	err := browser.Launch(
		"/usr/bin/chromium-browser",
		nil,
		nil,
	)

	if err != nil {
		log.Panic("ERROR: unable to launch browser: ", err)
	}

	go func() {
		tab, _ := browser.OpenTab(120 * time.Second)

		html, err := tab.Navigate("https://www.google.com", 120*time.Second)
		if err != nil {
			log.Panic("ERROR: unable to navigate to url", err)
		}
		fmt.Println(html)

		err = browser.CloseTab(tab, 120*time.Second)
		if err != nil {
			log.Panic("ERROR: unable to close tab", err)
		}

		err = browser.Terminate()
		if err != nil {
			log.Panic("ERROR: unable to terminate browser process", err)
		}
	}()

	browser.Wait()
}
