FROM ajitemsahasrabuddhe/ubuntu-base:bionic-1.0

LABEL maintainer="Ajitem Sahasrabuddhe <me@ajitem.com>"

# set non interactive frontend
ENV DEBIAN_FRONTEND noninteractive

RUN add-apt-repository ppa:canonical-chromium-builds/stage \
    && apt-get update \
    && apt-get install -y chromium-browser

# cleanup
RUN rm -r /var/lib/apt/lists/*

ADD bin/scraper /usr/bin/scraper

CMD ["scraper"]